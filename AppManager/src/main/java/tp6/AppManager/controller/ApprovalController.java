package tp6.AppManager.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import tp6.AppManager.entities.Approval;
import tp6.AppManager.exceptions.NotFoundException;
import tp6.AppManager.repositories.ApprovalRepository;

import java.util.Optional;

@RestController
@RequestMapping("/approvals")
public class ApprovalController {
    private final ApprovalRepository repository;
    public ApprovalController(ApprovalRepository repository) {
        this.repository = repository;
        repository.save(new Approval(1, Approval.ReponseType.APPROVED));
        repository.save(new Approval(2, Approval.ReponseType.REFUSED));
        repository.save(new Approval(3, Approval.ReponseType.REFUSED));
        repository.save(new Approval(4, Approval.ReponseType.APPROVED));
        repository.save(new Approval(5, Approval.ReponseType.REFUSED));
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Iterable<Approval> accounts(){
        try{
            return repository.findAll();
        } catch(Exception e){
            throw new RuntimeException();
        }
    }

    @RequestMapping(value="/{approval_id}", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Approval.ReponseType account (@PathVariable long approval_id){
        return repository.findById(approval_id).orElseThrow(() -> new NotFoundException("ID not found")).getReponse();
    }

    @RequestMapping(method= RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Approval addApproval(@RequestBody Approval a){
        try {
            return this.repository.save(a);
        } catch (Exception e){
            throw new RuntimeException();
        }
    }

    @RequestMapping(method= RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public long delApprouval(@RequestBody Approval a){
        try {
            this.repository.delete(a);
            return a.getId();
        } catch (Exception e){
            throw new NotFoundException(e.getMessage());
        }
    }
}
