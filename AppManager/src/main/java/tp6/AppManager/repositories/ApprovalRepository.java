package tp6.AppManager.repositories;

import org.springframework.data.repository.CrudRepository;
import tp6.AppManager.entities.Approval;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface ApprovalRepository extends CrudRepository<Approval, Long> {
    Optional<Approval> findById(Long id);
}

