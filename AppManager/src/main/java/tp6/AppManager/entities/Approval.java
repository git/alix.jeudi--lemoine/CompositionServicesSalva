package tp6.AppManager.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Approval {
    @Id
    private long approval_id;
    public enum ReponseType { APPROVED, REFUSED }
    @Column(name = "Reponse")
    private ReponseType reponse;

    public Approval(long approval_id, ReponseType reponse) {
        this.approval_id = approval_id;
        this.reponse = reponse;
    }

    public Approval(){}

    public long getId() {
        return approval_id;
    }

    public ReponseType getReponse() {
        return reponse;
    }
}
