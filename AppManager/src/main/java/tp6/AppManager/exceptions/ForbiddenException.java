package tp6.AppManager.exceptions;

public class ForbiddenException extends RuntimeException {
    public ForbiddenException(String e){
        super(e);
    }
}
