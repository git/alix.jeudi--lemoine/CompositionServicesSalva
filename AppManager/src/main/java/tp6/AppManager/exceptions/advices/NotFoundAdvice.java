package tp6.AppManager.exceptions.advices;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import tp6.AppManager.exceptions.NotFoundException;

@ControllerAdvice
public class NotFoundAdvice{
    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String bookHandler(NotFoundException ex) {
        return ex.getMessage();
    }
}

