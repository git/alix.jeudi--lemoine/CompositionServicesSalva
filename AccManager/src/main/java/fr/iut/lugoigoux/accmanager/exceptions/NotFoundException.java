package fr.iut.lugoigoux.accmanager.exceptions;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String exception) {
        super(exception);
    }
}
