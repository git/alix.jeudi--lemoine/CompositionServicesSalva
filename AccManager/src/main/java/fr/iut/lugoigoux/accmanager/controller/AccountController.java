package fr.iut.lugoigoux.accmanager.controller;

import fr.iut.lugoigoux.accmanager.exceptions.NotFoundException;
import fr.iut.lugoigoux.accmanager.entities.Account;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import fr.iut.lugoigoux.accmanager.repositories.AccountRepository;

@RestController
@RequestMapping("/accounts")
public class AccountController {
    private final AccountRepository repository;
    public AccountController(AccountRepository repository) {
        this.repository = repository;
        repository.save(new Account(1, 5000, Account.RiskType.LOW));
        repository.save(new Account(2, 2450, Account.RiskType.HIGH));
        repository.save(new Account(3, 1500, Account.RiskType.HIGH));
        repository.save(new Account(4, 10000, Account.RiskType.LOW));
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Iterable<Account> accounts(){
        try{
            return repository.findAll();
        } catch(Exception e){
            throw new RuntimeException();
        }
    }

    @RequestMapping(value="/{account_id}", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Account.RiskType account (@PathVariable long account_id){
        return repository.findById(account_id).orElseThrow(() -> new NotFoundException("ID not found")).getRisk();
    }

    @RequestMapping(method= RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public long addAccount(@RequestBody Account a){
        this.repository.save(a);
        try {
            return a.getId();
        } catch (Exception e){
            throw new RuntimeException();
        }
    }

}
