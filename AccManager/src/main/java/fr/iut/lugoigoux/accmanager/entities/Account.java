package fr.iut.lugoigoux.accmanager.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Account {
    @Id
    public long account_id;
    @Column(name = "somme")
    public float somme;
    public enum RiskType { LOW, HIGH }
    @Column(name = "risk")
    public RiskType risk;


    public Account(long account_id, float somme, RiskType risk) {
        this.account_id = account_id;
        this.somme = somme;
        this.risk = risk;
    }

    public Account() {}

    public long getId() {
        return account_id;
    }

    public RiskType getRisk() {
        return risk;
    }
}