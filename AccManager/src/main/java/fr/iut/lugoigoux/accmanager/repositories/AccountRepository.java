package fr.iut.lugoigoux.accmanager.repositories;

import fr.iut.lugoigoux.accmanager.entities.Account;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface AccountRepository extends CrudRepository<Account, Long> {
    Optional<Account> findById(long id);

}
