# Composition de services

## AccManager: Gestion des comptes bancaires (Springboot)
## AppManager: Gestion des réponses de prêt (Springboot)
## LoanApproval : Traitement des demandes de prêt (PHP Slim + Guzzle)